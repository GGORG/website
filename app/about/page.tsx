'use client';

import anime from 'animejs';
import { useEffect, useId } from 'react';

import Card from './Card';

export default function About(): JSX.Element {
  const cardClass = `${useId().replaceAll(':', '')}-bubble`;
  useEffect(() => {
    anime({
      targets: `.${cardClass}`,
      translateY: [60, 0],
      opacity: [0, 1],
      duration: 800,
      easing: 'easeOutElastic',
      delay: anime.stagger(80, { start: 300 }),
    });
  }, [cardClass]);
  return (
    <main className="flex items-center justify-center flex-col gap-2 lg:gap-5">
      <h1 className={`text-5xl lg:text-7xl ${cardClass}`}>About me</h1>
      <div className="flex flex-wrap items-center justify-center flex-col gap-2 lg:gap-4">
        <Card title="Name" text="Grzegorz" className={cardClass} />
        <Card title="Age" text={`${new Date().getFullYear() - 2009}`} className={cardClass} />
        <Card title="OS" text="Arch Linux" className={cardClass} />
        <Card title="Location" text="Wrocław, Poland" className={cardClass} />
        <Card title="Languages" text="Polish, English" className={cardClass} />
        <Card title="Pronouns" text="he/him" className={cardClass} />
        <Card title="Timezone" text="UTC+1" className={cardClass} />
        <Card title="Programming languages" text="JavaScript, TypeScript, Python, C, C++, C#, Java, Bash, HTML, CSS" className={cardClass} />
      </div>
    </main>
  );
}