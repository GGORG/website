export default function Card({
  title, text, className,
}: {
    title: string, text: string, className?: string,
}): JSX.Element {
  return (
    <div className={`flex flex-row flex-wrap items-center justify-center gap-1 lg:gap-2 bg-zinc-700 p-1 lg:p-3 rounded-2xl hover:bg-zinc-600 border border-zinc-600 hover:border-zinc-500 transition-colors duration-500 ${className ?? ''}`}>
      <h2 className="text-xl lg:text-2xl font-semibold inline">{title}</h2>
      <p className="text-lg lg:text-xl">{text}</p>
    </div>
  );
}