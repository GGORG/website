import Image from 'next/image';

import pfp from '../public/pfp.webp';
import { MessageBubbles, SocialButtons } from './HomePageAnimations';

export default function Home(): JSX.Element {
  return (
    <>
      <main className='flex items-center flex-col md:flex-row gap-4'>
        <Image
          src={pfp}
          alt="profile pic"
          width={256}
          height={256}
          className="w-32 md:h-64 h-32 md:w-64 rounded-3xl"
        />
        <MessageBubbles />
      </main>
      <SocialButtons />
    </>
  );
}
