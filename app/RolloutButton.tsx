import type { IconProp } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';

export default function RolloutButton({
  link, text, icon, className,
}: {
  link: string, text: string, icon: IconProp, className?: string
}): JSX.Element {
  return (
    <a
      href={link}
      className={`group bg-zinc-800 border border-zinc-600 rounded-3xl flex items-center w-8 h-8 md:w-16 md:h-16 overflow-hidden ease-in-out duration-300 transition-[width] shadow ${className ?? ''}`}
      target="_blank"
      rel="noreferrer"
      onMouseEnter={(e) => {
        e.currentTarget.style.width = `${e.currentTarget.scrollWidth}px`;
      }}
      onMouseLeave={(e) => {
        e.currentTarget.style.removeProperty('width');
      }}
    >
      <div className="flex w-8 h-8 md:w-16 md:h-16 flex-none items-center justify-center">
        <FontAwesomeIcon icon={icon} className="w-8 h-8 md:w-16 md:h-16" />
      </div>
      <span className="opacity-0 group-hover:opacity-100 transition-all duration-500 font-semibold pr-3 md:pr-6">{text}</span>
    </a>

  );
}