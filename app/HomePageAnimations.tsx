'use client';
import { faDiscord, faTelegram, faSpotify, faTwitter, faGithub, faGitlab, faRedditAlien, faMastodon } from '@fortawesome/free-brands-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import anime from 'animejs';
import { useId, useEffect } from 'react';

import MessageBubble from './MessageBubble';
import RolloutButton from './RolloutButton';

export function MessageBubbles(): JSX.Element {
  const bubbleClass = `${useId().replaceAll(':', '')}-bubble`;
  useEffect(() => {
    anime({
      targets: `.${bubbleClass}`,
      marginTop: ['2.5rem', 0],
      marginLeft: ['-2.5rem', 0],
      opacity: [0, 1],
      duration: 800,
      easing: 'easeOutElastic',
      delay: anime.stagger(1000),
    });
  }, [bubbleClass]);
  return (
    <div className='flex gap-3 flex-col overflow-hidden h-64 p-3'>
      <MessageBubble className={`text-2xl md:text-5xl ${bubbleClass}`}>
        Hi! <span className='inline-block animate-wave'>👋</span>
      </MessageBubble>
      <MessageBubble className={`text-xl md:text-3xl ${bubbleClass}`}>
        Welcome to my website!
      </MessageBubble>
      <MessageBubble className={`text-xl md:text-3xl ${bubbleClass}`}>
        I'm <span className="font-semibold">GGORG</span> and I'm a {new Date().getFullYear() - 2009} yo programmer.
      </MessageBubble>
    </div>
  );
}

export function SocialButtons(): JSX.Element {
  const buttonClass = `${useId().replaceAll(':', '')}-bubble`;
  useEffect(() => {
    anime({
      targets: `.${buttonClass}`,
      translateY: [30, 0],
      opacity: [0, 1],
      rotate: [-90, 0],
      duration: 800,
      easing: 'easeOutElastic',
      delay: anime.stagger(80, { start: 3000 }),
    });
  }, [buttonClass]);
  return (
    <div className="absolute w-screen bottom-2 md:bottom-10 left-0 flex flex-wrap justify-center gap-2 md:gap-4">
      <RolloutButton link="//discord.com/users/819845763848601611"
        icon={faDiscord} text="ggorg#9981" className={buttonClass} />
      <RolloutButton link="//t.me/GGORG0"
        icon={faTelegram} text="GGORG0" className={buttonClass} />
      <RolloutButton link="//open.spotify.com/user/nx4hkhjwh0p4vdu4zj2egr7vx?si=354da4aae1734eb5"
        icon={faSpotify} text="GGORG" className={buttonClass} />
      <RolloutButton link="//twitter.com/GGORG0"
        icon={faTwitter} text="GGORG0" className={buttonClass} />
      <RolloutButton link="//mastodon.social/@GGORG"
        icon={faMastodon} text="GGORG@mastodon.social" className={buttonClass} />
      <RolloutButton link="//reddit.com/u/GGORG0"
        icon={faRedditAlien} text="GGORG0" className={buttonClass} />
      <RolloutButton link="//github.com/GGORG0"
        icon={faGithub} text="GGORG0" className={buttonClass} />
      <RolloutButton link="//gitlab.com/GGORG"
        icon={faGitlab} text="GGORG" className={buttonClass} />
      <RolloutButton link="mailto:GGORG0@protonmail.com"
        icon={faEnvelope} text="GGORG0@protonmail.com" className={buttonClass} />
    </div>
  );
}
