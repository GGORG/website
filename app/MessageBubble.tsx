import React from 'react';

export default function MessageBubble({ children, className }: { children: React.ReactNode, className?: string }): JSX.Element {
  return (
    <div className={`bg-zinc-700 h-min w-fit p-3 rounded-3xl rounded-bl-sm ${className ?? ''}`}>
      <p>{children}</p>
    </div>
  );
}