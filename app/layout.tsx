'use client';

import './globals.css';
import { config } from '@fortawesome/fontawesome-svg-core';
import { Rubik as rubik } from '@next/font/google';

import '@fortawesome/fontawesome-svg-core/styles.css';
import NavBar from './NavBar';

config.autoAddCss = false;

const rubikFont = rubik({ subsets: ['latin'] });

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
  }): JSX.Element {
  return (
    <html lang="en" className={rubikFont.className}>
      <head />
      <body>
        <MainPageLayout>
          {children}
        </MainPageLayout>
        {/* eslint-disable-next-line jsx-a11y/control-has-associated-label, jsx-a11y/anchor-has-content */}
        <a rel="me" href="https://mastodon.social/@GGORG" className='hidden' />
      </body>
    </html>
  );
}

function MainPageLayout({
  children,
}: {
  children: React.ReactNode
}): JSX.Element {
  return (
    <div className="flex items-center justify-center w-screen h-screen bg-zinc-900 bg-gradient-to-b from-zinc-800 to-zinc-900 bg-cover bg-center text-zinc-50 overflow-hidden">
      <NavBar />
      {children}
    </div>
  );
}
