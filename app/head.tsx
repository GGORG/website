export default function Head(): JSX.Element {
  return (
    <>
      <title>GGORG</title>
      <meta name="description" content={`Just a ${new Date().getFullYear() - 2009} yo Polish programmer`} />
      <link rel="icon" href="/favicon.ico" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </>
  );
}