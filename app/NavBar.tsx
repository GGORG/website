'use client';

import type { IconProp } from '@fortawesome/fontawesome-svg-core';
import { faHome, faUser } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import anime from 'animejs';
import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { useEffect, useId } from 'react';

export default function NavBar(): JSX.Element {
  const barClass = `${useId().replaceAll(':', '')}-bar`;
  const isHome = usePathname() === '/';
  useEffect(() => {
    if (!isHome) { return; }
    anime({
      targets: `.${barClass}`,
      translateY: ['-200%', '0%'],
      duration: 800,
      easing: 'easeOutElastic',
      delay: 3100,
    });
  }, [barClass, isHome]);
  return (
    <nav className={`absolute w-screen top-2 md:top-10 left-0 flex flex-wrap justify-center gap-2 md:gap-4 ${barClass}`}>
      <NavBarButton link="/" text="Home" icon={faHome} />
      <NavBarButton link="/about" text="About" icon={faUser} />
    </nav>
  );
}

function NavBarButton({
  link, text, icon,
}: {
  link: string, text: string, icon: IconProp,
}): JSX.Element {
  const pathname = usePathname();
  const active = pathname === link;
  return (
    <Link href={link}
      className={`flex items-center gap-2 font-semibold px-2 md:px-4 py-1 md:py-2 rounded transition-colors duration-200 border-b-2 ${active ? 'border-amber-500 bg-amber-700 hover:bg-amber-600 hover:border-amber-400' : 'border-zinc-500 bg-zinc-700 hover:bg-zinc-600 hover:border-zinc-400'}`}>
      <FontAwesomeIcon icon={icon} />
      <p>{text}</p>
    </Link>
  );
}