/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './app/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      fontFamily: {
        sans: ['Rubik', 'Arial', 'sans-serif'],
      },
      keyframes: {
        wave: {
          '0%, 60%, to': { 'transform': 'rotate(0deg)' },
          '10%, 30%': { 'transform': 'rotate(14deg)' },
          '20%': { 'transform': 'rotate(-8deg)' },
          '40%': { 'transform': 'rotate(-4deg)' },
          '50%': { 'transform': 'rotate(10deg)' },
        },
      },
      animation: {
        'wave': 'wave 2.5s ease-in-out 1s infinite',
      },
    },
  },
  plugins: [],
};
