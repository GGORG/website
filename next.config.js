/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  eslint: {
    ignoreDuringBuilds: true,
    dirs: ['app'],
  },
  experimental: {
    appDir: true,
  },
};

module.exports = nextConfig;
