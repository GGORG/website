const { createConfig } = require('eslint-config-galex/dist/createConfig');
const { getDependencies } = require('eslint-config-galex/dist/getDependencies');
const {
  createReactOverride,
} = require('eslint-config-galex/dist/overrides/react');

const dependencies = getDependencies();

const customReactOverride = createReactOverride({
  ...dependencies,
  plugins: ['tailwindcss'],
  extends: [
    'plugin:tailwindcss/recommended',
  ],
  rules: {
    'import/no-default-export': 'off',
  },
});

module.exports = createConfig({
  overrides: [customReactOverride],
  rules: {
    'semi': ['error', 'always'],
    'quotes': ['warn', 'single'],
    'comma-dangle': ['warn', 'always-multiline'],
  },
});
